/*
 * Sample node.js code for AWS Lambda to upload the JSON documents
 * pushed from DynamoDB to Amazon Elasticsearch.
 *
 *
 * Copyright 2015- Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Amazon Software License (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at http://aws.amazon.com/asl/
 * or in the "license" file accompanying this file.  This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
 * express or implied.  See the License for the specific language governing
 * permissions and limitations under the License.
 */

/* == Imports == */
var AWS = require('aws-sdk');
var path = require('path');

/* == Globals == */
var esDomain = {
    region: 'us-east-1',
    endpoint: 'search-demomill-ugabuduyzn4hi54etn3k4svn2e.us-east-1.es.amazonaws.com',
    index: 'users',
    doctype: 'user'
};
var endpoint = new AWS.Endpoint(esDomain.endpoint);
/*
 * The AWS credentials are picked up from the environment.
 * They belong to the IAM role assigned to the Lambda function.
 * Since the ES requests are signed using these credentials,
 * make sure to apply a policy that allows ES domain operations
 * to the role.
 */
var creds = new AWS.EnvironmentCredentials('AWS');


/* Lambda "main": Execution begins here */
exports.handler = function(event, context) {
    console.log(JSON.stringify(event, null, '  '));
    event.Records.forEach(function(record) {
        var jsonDoc = JSON.stringify(record);
        var operation = record.eventName;
        var method;
        var dynamoRecord;
        if (operation == 'INSERT')
        {
            method = 'POST';
            dynamoRecord = JSON.stringify(record.dynamodb.NewImage);
        }
        if (operation == 'MODIFY')
        {
            method = 'PUT';
            dynamoRecord = JSON.stringify(record.dynamodb.NewImage);
        }
        if (operation == 'REMOVE')
        {
            method = 'DELETE';
            dynamoRecord = '';
        }
        console.log("Calling " + method + " with: " + dynamoRecord.toString());
        var username = record.dynamodb.Keys.username.S;
        postToES(dynamoRecord.toString(), username, method, context);
    });
}


/*
 * Post the given document to Elasticsearch
 */
function postToES(doc, docname, operation, context) {
    var req = new AWS.HttpRequest(endpoint);

    req.method = operation;
    req.path = path.join('/', esDomain.index, esDomain.doctype, docname);
    req.region = esDomain.region;
    req.headers['presigned-expires'] = false;
    req.headers['Host'] = endpoint.host;
    if (operation != 'DELETE')
    {
        req.body = doc;
    }

    var signer = new AWS.Signers.V4(req , 'es');  // es: service code
    signer.addAuthorization(creds, new Date());

    var send = new AWS.NodeHttpClient();
    send.handleRequest(req, null, function(httpResp) {
        var respBody = '';
        httpResp.on('data', function (chunk) {
            respBody += chunk;
        });
        httpResp.on('end', function (chunk) {
            console.log('Response: ' + respBody);
            context.succeed('Lambda added document ' + doc);
        });
    }, function(err) {
        console.log('Error: ' + err);
        context.fail('Lambda failed with error ' + err);
    });
}